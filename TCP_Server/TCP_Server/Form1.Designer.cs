﻿namespace TCP_Server
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuItem_startListen = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItem_closeListen = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip_tbx_port = new System.Windows.Forms.ToolStripTextBox();
            this.rtbx_log = new System.Windows.Forms.RichTextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItem_startListen,
            this.MenuItem_closeListen,
            this.设置ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuItem_startListen
            // 
            this.MenuItem_startListen.Name = "MenuItem_startListen";
            this.MenuItem_startListen.Size = new System.Drawing.Size(68, 21);
            this.MenuItem_startListen.Text = "开始监听";
            this.MenuItem_startListen.Click += new System.EventHandler(this.MenuItem_startListen_Click);
            // 
            // MenuItem_closeListen
            // 
            this.MenuItem_closeListen.Name = "MenuItem_closeListen";
            this.MenuItem_closeListen.Size = new System.Drawing.Size(68, 21);
            this.MenuItem_closeListen.Text = "停止监听";
            this.MenuItem_closeListen.Click += new System.EventHandler(this.MenuItem_closeListen_Click);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStrip_tbx_port});
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.设置ToolStripMenuItem.Text = "设置";
            // 
            // toolStrip_tbx_port
            // 
            this.toolStrip_tbx_port.Font = new System.Drawing.Font("Microsoft YaHei UI", 9F);
            this.toolStrip_tbx_port.Name = "toolStrip_tbx_port";
            this.toolStrip_tbx_port.Size = new System.Drawing.Size(100, 23);
            this.toolStrip_tbx_port.Text = "6000";
            // 
            // rtbx_log
            // 
            this.rtbx_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbx_log.Location = new System.Drawing.Point(0, 25);
            this.rtbx_log.Name = "rtbx_log";
            this.rtbx_log.Size = new System.Drawing.Size(800, 425);
            this.rtbx_log.TabIndex = 1;
            this.rtbx_log.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.rtbx_log);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_startListen;
        private System.Windows.Forms.ToolStripMenuItem MenuItem_closeListen;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStrip_tbx_port;
        private System.Windows.Forms.RichTextBox rtbx_log;
    }
}

